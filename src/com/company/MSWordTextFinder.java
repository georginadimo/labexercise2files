package com.company;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MSWordTextFinder {
    public ArrayList<String> findTextInWordDocument(String path, List<String> listofstrings)throws IOException {
        ArrayList<String> list = new ArrayList<String>();

        for (String eachstring : listofstrings){
            boolean found=false;
            int line_found=1;
            int line_count = 1;
            XWPFDocument docx = new XWPFDocument(new FileInputStream(path));

            //using XWPFWordExtractor Class
            XWPFWordExtractor we = new XWPFWordExtractor(docx);
            String data=we.getText();

            String[] lines = data.split("\\r?\\n");
            for (String line : lines) {
                if (line.contains(eachstring)){
                    found=true;
                    line_found=line_count;
                }
                line_count=line_count+1;
            }

            if (found==true){
                String stringtoprint="Word:'"+eachstring+"',Found:YES,line:"+line_found;
                list.add(stringtoprint);

            }
            else
            {
                String stringtoprint="Word:'"+eachstring+"',Found:No,line:-";
                list.add(stringtoprint);
            }

        }

        return list;
    }
}
